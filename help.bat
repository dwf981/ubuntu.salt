@echo off
REM v0.1.1 This script updates the Claymore miner
setlocal enabledelayedexpansion

echo.
echo salt '*' cmd.run 'salt\\help'      - Shows this help
echo salt '*' cmd.run 'salt\\status'    - Displays status info
echo salt '*' cmd.run 'salt\\update'    - Selects a different Claymore version
echo salt '*' cmd.run 'salt\\putconf'   - Selects a configuration for Claymore
echo salt '*' cmd.run 'salt\\pull'      - Pulls latest changes from git
echo salt '*' cmd.run 'salt\\setboot'   - Set booting device: 0 - Ubuntu, 4 - Windows 10, 5 - EthOS
