@echo off
REM v0.1.1 This script updates the Claymore miner
setlocal enabledelayedexpansion

echo.
set COUNT=0
for /r %%i in (kit\*.zip) do (
  set /a COUNT=COUNT+1
  echo !COUNT!^) %%i
)
echo.

:: Set input as the first parameter or read it if curent user is Administrator
set INPUT=100
if "%1"=="" (if "%USERNAME%"=="Administrator" (set /P INPUT="Select a zip file: ") else (exit /b 0)) else (set INPUT=%1)

set COUNT=0
for /r %%i in (kit\*.zip) do (
  set /a COUNT=COUNT+1
  if "!COUNT!" EQU "!INPUT!" (
    echo Selected file: %%i
    rd /s /q "C:\Claymore"
    md "C:\Claymore"
    choco install unzip -yes
    pushd C:\Claymore
    copy "%%i" Claymore.zip
    unzip Claymore.zip
    del Claymore.zip
    move "Claymore*" "Claymore"
    cd "Claymore"
    ::ls -al
    del config.txt
    del epools.txt
    del dpools.txt
    popd
    :: stop Claymore
    tasklist /FI "IMAGENAME eq EthDcrMiner64.exe" 2>NUL | find /I /N "EthDcrMiner64.exe">NUL
    if "%ERRORLEVEL%"=="0" ( tskill EthDcrMiner64 2>NUL && ping localhost -n 2 >NUL)
    tasklist /FI "IMAGENAME eq MinerHanger.exe" 2>NUL | find /I /N "MinerHanger.exe">NUL
    if "%ERRORLEVEL%"=="0" ( tskill MinerHanger 2>NUL && ping localhost -n 2 >NUL)
    :: copy to Desktop
    xcopy /S /R /H /I /Y /Q "C:\Claymore\Claymore" "C:\Users\Administrator\Desktop\Claymore"
    xcopy /S /R /H /I /Y /Q "C:\Claymore\Claymore" "C:\Users\Administrator\OneDrive\Claymore"
    rd /s /q "C:\Claymore"
  )
)
rem @powershell -executionpolicy remotesigned "& ""%~dpn0.ps2"""
