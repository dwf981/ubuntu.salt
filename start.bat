@echo off
set RIGNAME=UNNAMED
set EPOOL=nanopool
set DPOOL=coinmine
set MAIL=octavian.cota@cdev.ro
set EWAL=0x2f50A518F41c9226b4a1cC2C79337332FB305815
set DWAL=dwf
set DUAL=Y

REM epool = nanopool, ethpool, ethermine
REM dpool = coinmine

REM // use ethermine
set EPOOL=ethermine

REM // poloniex ETH wallet
::set EWAL=0x48fc28c1b130bd30ee58e540eb779751ca1d44e8

:: initialize RIGNAME with MAC address
::for /f %%A in ('getmac^|findstr /V disconnected^|findstr /C:-') do ( set RIGNAME=%%A && goto :FOR_END )
::FOR_END

if exist "%~dp0downloadConfig.bat" (
  call "%~dp0downloadConfig.bat"
)

if exist "C:\Users\Administrator\Desktop\config.bat" (
  call "C:\Users\Administrator\Desktop\config.bat"
)

::set /A "RIGNAME=!RIGNAME:-=x!"
if "%1" NEQ "" (
  set /P RIGNAME="RIGNAME(%RIGNAME%): "
  set /P DUAL="DUAL Y/N (%DUAL%): "
  set /P MAIL="MAIL(%MAIL%): "
  set /P EPOOL="EPOOL - Ethereum pool name (%EPOOL%): "
  set /P DPOOL="DPOOL - Decred pool name (%DPOOL%): "
  set /P EWAL="EWAL - Ethereum wallet address (%EWAL%): "
  set /P DWAL="DWAL - %DPOOL% username (%DWAL%): "
)

set DWAL=%DWAL%.%RIGNAME%
set C=C:\Users\Administrator\Desktop\Claymore\config.txt
set E=C:\Users\Administrator\Desktop\Claymore\epools.txt
set D=C:\Users\Administrator\Desktop\Claymore\dpools.txt

:: stop Claymore
tasklist /FI "IMAGENAME eq EthDcrMiner64.exe" 2>NUL | find /I /N "EthDcrMiner64.exe">NUL
if "%ERRORLEVEL%"=="0" ( tskill EthDcrMiner64 && ping localhost -n 2 >NUL)
tasklist /FI "IMAGENAME eq MinerHanger.exe" 2>NUL | find /I /N "MinerHanger.exe">NUL
if "%ERRORLEVEL%"=="0" ( tskill MinerHanger && ping localhost -n 2 >NUL)

xcopy /S /R /H /I /Y /Q "C:\Users\Administrator\OneDrive\Claymore" "C:\Users\Administrator\Desktop\Claymore"

set COUNT=0
IF /I "%EPOOL%" EQU "nanopool" ( call :GEN_CONFIG_NANOPOOL && set /A COUNT=%COUNT%+1 )
IF /I "%EPOOL%" EQU "ethpool" ( call :GEN_CONFIG_ETHPOOL && set /A COUNT=%COUNT%+1 )
IF /I "%EPOOL%" EQU "ethermine" ( call :GEN_CONFIG_ETHERMINE && set /A COUNT=%COUNT%+1 )
IF "%COUNT%" NEQ "1" ( echo Unsupported epool %EPOOL% && goto :EOF )
IF /I "%DPOOL%" EQU "coinmine" ( call :GEN_CONFIG_COINMINE && set /A COUNT=%COUNT%+1 )
IF "%COUNT%" NEQ "2" ( echo Unsupoorted dpool %EPOOL% && goto :EOF )
call :GEN_CONFIG_END
:DONE_EPOOL

%~dp0Bginfo.exe /silent /timer:0

:: Start Claymore
::echo test > "%USERPROFILE%\Desktop\test1.txt"
pushd "C:\Users\Administrator\Desktop\Claymore"
start MinerHanger.exe
set ERRORLEVEL=0
EthDcrMiner64.exe
echo ERRORLEVEL %ERRORLEVEL%
popd
pause
goto :EOF

:GEN_CONFIG_NANOPOOL
  echo -epool eth-eu1.nanopool.org:9999> %C%
  echo -ewal %EWAL%.%RIGNAME%/%MAIL%>> %C%
  echo -epsw x>> %C%
  echo POOL: eth-eu2.nanopool.org:9999, WALLET: %EWAL%.%RIGNAME%/%MAIL%, PSW: x, WORKER: , ESM: 0, ALLPOOLS: ^0> %E%
  echo POOL: eth-asia1.nanopool.org:9999, WALLET: %EWAL%.%RIGNAME%/%MAIL%, PSW: x, WORKER: , ESM: 0, ALLPOOLS: ^0>> %E%
  echo POOL: eth-us-east1.nanopool.org:9999, WALLET: %EWAL%.%RIGNAME%/%MAIL%, PSW: x, WORKER: , ESM: 0, ALLPOOLS: ^0>> %E%
  echo POOL: eth-us-west1.nanopool.org:9999, WALLET: %EWAL%.%RIGNAME%/%MAIL%, PSW: x, WORKER: , ESM: 0, ALLPOOLS: ^0>> %E%
exit /b 0

:GEN_CONFIG_ETHPOOL
  echo -epool eu1.ethpool.org:3333> %C%
  echo -ewal %EWAL%.%RIGNAME%>> %C%
  echo -epsw x>> %C%
  echo #> %E%
exit /b 0

:GEN_CONFIG_ETHERMINE
  echo -epool eu1.ethermine.org:4444> %C%
  echo -ewal %EWAL%.%RIGNAME%>> %C%
  echo -epsw x>> %C%
  echo #> %E%
exit /b 0

:GEN_CONFIG_COINMINE
  echo -dpool dcr.coinmine.pl:2222>> %C%
  echo -dwal dwf.%RIGNAME%>> %C%
  echo -dpsw x>> %C%
  echo #> %D%
exit /b 0

:GEN_CONFIG_END
  echo -ftime ^10>> %C%
  echo -esm ^1>> %C%
  if /I "%DUAL%" EQU "Y" (echo -mode ^0>>%C%) else (echo -mode ^1>>%C%)
  echo -tt ^70>> %C%
  echo -r ^1>> %C%
  echo -asm ^1>> %C%
  echo -mport ^3333>> %C%
  echo -mpsw x>> %C%
  echo -dcri ^30>> %C%
  echo -retrydelay ^2>> %C%
  echo -minspeed ^50>> %C%
exit /b 0
