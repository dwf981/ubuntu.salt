@echo off
REM v0.1.1 This script updates the Claymore miner
setlocal enabledelayedexpansion

echo.
set COUNT=0
for /r %%i in (config\*.zip) do (
  set /a COUNT=COUNT+1
  echo !COUNT!^) %%i
)
echo.

:: Set input as the first parameter or read it if curent user is Administrator
set INPUT=100
if "%1"=="" (if "%USERNAME%"=="Administrator" (set /P INPUT="Select a zip file: ") else (exit /b 0)) else (set INPUT=%1)

set COUNT=0
for /r %%i in (config\*.zip) do (
  set /a COUNT=COUNT+1
  if "!COUNT!" EQU "!INPUT!" (
    echo Selected file: %%i
    rd /s /q "C:\Claymore" 2>NUL
    md "C:\Claymore" 2>NUL
    choco install unzip -yes >NUL
    pushd C:\Claymore
    copy "%%i" Claymore.zip
    unzip Claymore.zip
    del Claymore.zip
    move "Claymore*" "Claymore"
    if exist "%~dp0downloadConfig.bat" ( call "%~dp0downloadConfig.bat" )
    if exist "C:\Users\Administrator\Desktop\config.bat" ( call "C:\Users\Administrator\Desktop\config.bat" )
    echo.
    echo Identified as: !RIGNAME!
    sed s/RIG/!RIGNAME!/g C:\Claymore\Claymore\config.txt > C:\Users\Administrator\Desktop\Claymore\config.txt
    sed s/RIG/!RIGNAME!/g C:\Claymore\Claymore\epools.txt > C:\Users\Administrator\Desktop\Claymore\epools.txt
    sed s/RIG/!RIGNAME!/g C:\Claymore\Claymore\dpools.txt > C:\Users\Administrator\Desktop\Claymore\dpools.txt
    rd /s /q "C:\Claymore" 2>NUL
    echo.
    call "%~dp0status.bat"
    rem xcopy /S /R /H /I /Y /Q "C:\Claymore\Claymore" "C:\Users\Administrator\Desktop\Claymore"
  )
)
