@echo off
md "C:\Program Files\OpenVPN" >NUL 2>&1
md "C:\Program Files\OpenVPN\bin" >NUL 2>&1
md "C:\Program Files\OpenVPN\config" >NUL 2>&1

echo @tasklist /FI "IMAGENAME eq openvpn-gui.exe" 2^>NUL ^| find /I /N "openvpn-gui.exe"^>NUL >"C:\Program Files\OpenVPN\bin\openvpn-gui.bat"
echo @if "%%ERRORLEVEL%%" NEQ "0" start /d "%%~dp0" openvpn-gui.exe --connect rig.ovpn >>"C:\Program Files\OpenVPN\bin\openvpn-gui.bat"

schtasks /delete /tn OpenVPNStart /F >NUL 2>&1
schtasks /create /sc onlogon /tn OpenVPNStart /rl highest /tr "C:\Program Files\OpenVPN\bin\openvpn-gui.bat"
