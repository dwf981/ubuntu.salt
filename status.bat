@echo off
REM v0.1.2 - show status

echo.
echo # Claymore:
findstr /b ^v C:\Users\Administrator\Desktop\Claymore\History.txt | head -n1

echo.
echo # Pools:
findstr "^-epool ^-dpool ^-ewal ^-dwal ^-eworker" C:\Users\Administrator\Desktop\Claymore\config.txt
