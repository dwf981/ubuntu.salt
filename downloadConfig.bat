@echo off
REM v0.1.2 This script generates config.bat on Desktop
setlocal enabledelayedexpansion
set COUNTER=0
set MAXTRY=50
:RETRY
  set MACS=
  set /A COUNTER=COUNTER+1
  if "!COUNTER!" GTR "!MAXTRY!" goto :DONE
  for /f "tokens=1" %%i in ('getmac.exe ^| findstr /C:"-" ^| findstr /V "Media"') do if "!MACS!"=="" (SET MACS=%%i) else (SET MACS=!MACS!;%%i)
  set URL=http://gnom.ro/miner/config.php?mac=!MACS!
  if "!COUNTER!" EQU "1" (echo Sending mac addesses ^(!URL!^).. ) else (echo ^[!COUNTER!/!MAXTRY!^] Sending mac addesses ^(!URL!^).. )
  powershell -executionpolicy remotesigned -command "(new-object System.Net.WebClient).DownloadFile('!URL!', 'C:\Users\Administrator\Desktop\config.bat')"
  if not "%ERRORLEVEL%" EQU "0" (ping 127.0.0.1 -n ^2>NUL && goto :RETRY)
:DONE
if "%ERRORLEVEL%" EQU "0" (echo Finished.) else (echo ERR: Failed to generate config.bat, tried !COUNTER! times ^(%ERRORLEVEL%^))
::pause
