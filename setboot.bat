@echo off
IF /I "%1" EQU "0" ( copy "%~dp0grubenv0" "U:\boot\grub\grubenv" && echo Installed Ubuntu && pause && goto :EOF )
IF /I "%1" EQU "4" ( copy "%~dp0grubenv4" "U:\boot\grub\grubenv" && echo Installed Windows 10 && pause && goto :EOF )
IF /I "%1" EQU "5" ( copy "%~dp0grubenv5" "U:\boot\grub\grubenv" && echo Installed EthOS && pause && goto :EOF )
echo setboot accepts one numeric parameter:
echo   0 - Ubuntu
echo   4 - Windows 10
echo   5 - EthOS
echo.
::pause
goto :EOF
