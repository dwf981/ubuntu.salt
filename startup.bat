@echo off
REM v0.1.1 This script sets Claymore to start automatically
setlocal enabledelayedexpansion

cmd /c dir "C:\Users\Administrator\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\*.lnk"
echo.

set /p YN="Remove Claymore from Startup? (y/n) : "
if /i "!YN!"=="Y" ( echo x ) else ( echo y )

set /p YN="Add Claymore to Startup? (y/n) : "
if /i "!YN!"=="Y" ( echo x ) else ( echo y )
